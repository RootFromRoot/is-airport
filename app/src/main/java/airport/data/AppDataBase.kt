package airport.data


import airport.data.dao.TicketDAO
import airport.data.model.Ticket
import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

@Database(entities = [Ticket::class], version = 24)
//@TypeConverters(DateTypeConverter::class)
abstract class AppDataBase : RoomDatabase() {
    abstract fun ticketDao(): TicketDAO
}