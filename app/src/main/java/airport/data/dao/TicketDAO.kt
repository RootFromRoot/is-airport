package airport.data.dao

import airport.data.model.Ticket
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query

@Dao
interface TicketDAO {
    @Query("SELECT * from ticket WHERE id =:id")
    fun getById(id: Int): Ticket

    @Query("SELECT * from ticket")
    fun getAll(): List<Ticket>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(song: Ticket)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(song: List<Ticket>)

    @Query("DELETE from ticket")
    fun deleteAll()

    @Query("DELETE from ticket WHERE id = :id")
    fun delete(id: Int)
}
