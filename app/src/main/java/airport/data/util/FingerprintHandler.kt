package airport.data.util

import android.hardware.fingerprint.FingerprintManager
import android.os.CancellationSignal

class FingerprintHandler(private val listener: FingerprintHelperListener) : FingerprintManager.AuthenticationCallback() {

    private var cancellationSignal: CancellationSignal? = null

    fun startAuth(manager: FingerprintManager, cryptoObject: FingerprintManager.CryptoObject) {
        cancellationSignal = CancellationSignal()

        try {
            manager.authenticate(cryptoObject, cancellationSignal, 0, this, null)
        } catch (ex: SecurityException) {
            listener.authenticationFailed("An error occurred: " + ex.message)
        } catch (ex: Exception) {
            listener.authenticationFailed("An error occurred: " + ex.message)
        }

    }

    fun cancel() {
        if (cancellationSignal != null)
            cancellationSignal!!.cancel()
    }

    interface FingerprintHelperListener {
        fun authenticationFailed(error: String)
        fun authenticationSuccess(result: FingerprintManager.AuthenticationResult)
    }

   override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
        listener.authenticationFailed("AuthenticationError : $errString")
    }

     override fun onAuthenticationHelp(helpCode: Int, helpString: CharSequence) {
        listener.authenticationFailed("AuthenticationHelp : $helpString")
    }

    override fun onAuthenticationSucceeded(result: FingerprintManager.AuthenticationResult) {
        listener.authenticationSuccess(result)
    }

    override fun onAuthenticationFailed() {
        listener.authenticationFailed("Authentication Failed!")
    }
}

