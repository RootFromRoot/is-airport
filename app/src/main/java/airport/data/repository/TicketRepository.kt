package airport.data.repository

import airport.data.dao.TicketDAO
import airport.data.model.Ticket
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class TicketRepository(private val dao: TicketDAO) {
    fun getAll() = Observable.fromCallable { dao.getAll() }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

    fun insert(ticket: Ticket) = Observable.fromCallable { dao.insert(ticket) }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

    fun delete(id: Int) = Observable.fromCallable { dao.delete(id) }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

    fun getById(id: Int) = Observable.fromCallable { dao.getById(id) }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}