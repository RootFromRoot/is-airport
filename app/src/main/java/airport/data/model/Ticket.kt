package airport.data.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity(    tableName = "ticket")

data class Ticket(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int? = null,

    @ColumnInfo(name = "ticket_number")
    val ticketNumber: String,

    @ColumnInfo(name ="department_time")
    val departmentTime: String,

    @ColumnInfo(name ="department_place")
    val departmentPlace: String,

    @ColumnInfo(name ="carrier")
    val carrier: String,

    @ColumnInfo(name ="terminal")
    val terminal: String,

    @ColumnInfo(name ="gate")
    val gate: String,


    @ColumnInfo(name ="arrival_time")
    val arrivalTime: String,

    @ColumnInfo(name ="arrival_place")
    val arrivalPlace: String,


    @ColumnInfo(name = "baggage_id")
    val baggageId: Int?
) {

}
