package airport.ui.presenter

import airport.data.repository.TicketRepository
import airport.ui.view.FlightInfoView
import airport.ui.view.MainView

interface FlightInfoPresenter {
    val view: FlightInfoView
    val repository: TicketRepository
    fun bind(view: FlightInfoView)

}