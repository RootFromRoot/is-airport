package airport.ui.presenter.implementation

import airport.ui.presenter.BorderControlPresenter
import airport.ui.view.BorderCtrlView
import android.Manifest
import android.app.KeyguardManager
import android.content.Context
import android.content.pm.PackageManager
import android.hardware.fingerprint.FingerprintManager
import android.os.CancellationSignal
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyPermanentlyInvalidatedException
import android.security.keystore.KeyProperties
import android.support.v4.app.ActivityCompat
import android.widget.Toast
import java.io.IOException
import java.security.*
import java.security.cert.CertificateException
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.NoSuchPaddingException
import javax.crypto.SecretKey

const val KEY_NAME = "example_key"

class BorderControlImpl : BorderControlPresenter {
    override lateinit var view: BorderCtrlView

    override lateinit var fingerprintManager: FingerprintManager
    override lateinit var keyguardManager: KeyguardManager
    override lateinit var keyStore: KeyStore
    override lateinit var keyGenerator: KeyGenerator
    override lateinit var cipher: Cipher
    override lateinit var cryptoObject: FingerprintManager.CryptoObject
    override lateinit var cancellationSignal: CancellationSignal

    override fun bind(view: BorderCtrlView) {
        this.view = view
    }

    override fun getManagers(): Boolean {
        keyguardManager = view.activity.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
        fingerprintManager = view.activity.getSystemService(Context.FINGERPRINT_SERVICE) as FingerprintManager
        if (!fingerprintManager.isHardwareDetected) {


            if (!keyguardManager.isKeyguardSecure) {
                Toast.makeText(
                        view.activity.applicationContext,
                        "Lock screen security not enabled in Settings",
                        Toast.LENGTH_LONG
                )
                        .show()
                return false
            }

            if (ActivityCompat.checkSelfPermission(
                            view.activity.applicationContext,
                            Manifest.permission.USE_FINGERPRINT
                    )
                    != PackageManager.PERMISSION_GRANTED
            ) {
                Toast.makeText(
                        view.activity.applicationContext,
                        "Fingerprint authentication permission not enabled", Toast.LENGTH_LONG
                ).show()
                return false
            }

            if (!fingerprintManager.hasEnrolledFingerprints()) {
                Toast.makeText(
                        view.activity.applicationContext, "Register at least one fingerprint in Settings", Toast.LENGTH_LONG
                ).show()
                return false
            }
        } else {
            Toast.makeText(
                    view.activity.applicationContext, "Fingerprint Scanner isn`t detected", Toast.LENGTH_LONG
            ).show()
        }
        return true

    }

    override fun generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore")
        } catch (e: NoSuchAlgorithmException) {
            throw RuntimeException("Failed to get KeyGenerator instance", e)
        } catch (e: NoSuchProviderException) {
            throw RuntimeException("Failed to get KeyGenerator instance", e)
        }

        try {
            keyStore.load(null)
            keyGenerator.init(
                    KeyGenParameterSpec.Builder(
                            KEY_NAME,
                            KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
                    )
                            .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                            .setUserAuthenticationRequired(true)
                            .setEncryptionPaddings(
                                    KeyProperties.ENCRYPTION_PADDING_PKCS7
                            )
                            .build()
            )
            keyGenerator.generateKey()
        } catch (e: NoSuchAlgorithmException) {
            throw RuntimeException(e)
        } catch (e: InvalidAlgorithmParameterException) {
            throw RuntimeException(e)
        } catch (e: CertificateException) {
            throw RuntimeException(e)
        } catch (e: IOException) {
            throw RuntimeException(e)
        }
    }

    override fun cipherInit(): Boolean {
        try {
            cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/"
                            + KeyProperties.BLOCK_MODE_CBC + "/"
                            + KeyProperties.ENCRYPTION_PADDING_PKCS7
            )
        } catch (e: NoSuchAlgorithmException) {
            throw RuntimeException("Failed to get Cipher", e)
        } catch (e: NoSuchPaddingException) {
            throw RuntimeException("Failed to get Cipher", e)
        }

        try {
            keyStore.load(null)
            val key = keyStore.getKey(KEY_NAME, null) as SecretKey
            cipher.init(Cipher.ENCRYPT_MODE, key)
            return true
        } catch (e: KeyPermanentlyInvalidatedException) {
            return false
        } catch (e: KeyStoreException) {
            throw RuntimeException("Failed to init Cipher", e)
        } catch (e: CertificateException) {
            throw RuntimeException("Failed to init Cipher", e)
        } catch (e: UnrecoverableKeyException) {
            throw RuntimeException("Failed to init Cipher", e)
        } catch (e: IOException) {
            throw RuntimeException("Failed to init Cipher", e)
        } catch (e: NoSuchAlgorithmException) {
            throw RuntimeException("Failed to init Cipher", e)
        } catch (e: InvalidKeyException) {
            throw RuntimeException("Failed to init Cipher", e)
        }
    }
}