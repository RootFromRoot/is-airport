package airport.ui.presenter.implementation

import airport.data.repository.TicketRepository
import airport.data.service.Application
import airport.ui.presenter.FlightInfoPresenter
import airport.ui.view.FlightInfoView
import airport.ui.view.MainView

class FlightInfoImpl: FlightInfoPresenter {
    override lateinit var view: FlightInfoView
    override lateinit var repository: TicketRepository
    override fun bind(view: FlightInfoView) {
        this.view = view
        repository = TicketRepository((view.activity.application as Application).db.ticketDao())
    }

}