package airport.ui.presenter.implementation

import airport.data.model.Ticket

import airport.data.repository.TicketRepository
import airport.data.service.Application
import airport.ui.activity.first_level.FlightInfoActivity
import airport.ui.presenter.MainPresenter
import airport.ui.view.MainView
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import java.text.SimpleDateFormat
import java.util.*

class MainPresenterImpl : MainPresenter {
    override lateinit var view: MainView
    override lateinit var repository: TicketRepository


    @SuppressLint("CheckResult")
    override fun bind(view: MainView) {
        val currentDate = SimpleDateFormat("dd/MM/yyyy").format(Date())

        if (currentDate == "20/02/2019"){
        this.view = view
        repository = TicketRepository((view.activity.application as Application).db.ticketDao())
        repository.getAll().subscribe { fillDB() }
        }
    }

    override fun fillDB() {

        repository.insert(
            Ticket(
                null,
                "11111",
                "18.01.2019 20-10",
                "Kyiv",
                "Ryanair",
                "D",
                "D8",
                "19.01.2019 08-30",
                "Dubai",
                11111
            )
        ).subscribe()
        repository.insert(
            Ticket(
                null,
                "22222",
                "18.01.2019 20-10",
                "Kyiv",
                "London",
                "D",
                "D2",
                "19.01.2019 18-30",
                "Dubai",
                11111
            )
        ).subscribe()
        repository.insert(
            Ticket(
                null,
                "33333",
                "18.01.2019 20-10",
                "Kyiv",
                "Berlin",
                "D",
                "D9",
                "18.01.2019 23-30",
                "Dubai",
                11111
            )
        ).subscribe()
    }

    override fun startFlightActivity(context: Context, ticket: Ticket) {
        view.activity.startActivity(Intent(context, FlightInfoActivity::class.java).putExtra("id", ticket.id))
    }
}

