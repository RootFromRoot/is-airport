package airport.ui.presenter

import airport.data.model.Ticket

import airport.data.repository.TicketRepository
import airport.ui.view.MainView
import android.content.Context

interface MainPresenter {
    var view: MainView
    var repository: TicketRepository


    fun fillDB()
    fun bind(view: MainView)
    fun startFlightActivity(context: Context, ticket: Ticket)

}