package airport.ui.presenter

import airport.airport.R
import airport.ui.util.assertState
import airport.ui.view.PassportControlView
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolygonOptions
import com.google.android.gms.maps.model.PolylineOptions

interface GoogleMapPresenter {
    fun bind(activity: AppCompatActivity)
    fun setMap(map: GoogleMap)
    fun getMap(): GoogleMap

    fun drawRouteFromMyLocationToPoint(point: LatLng)
    fun drawRouteFromPointToPoint(pointStart: LatLng, pointDestination: LatLng)
    fun drawRouteByLocationPoints(points: List<LatLng>)

    fun runAssertSequence()
    fun assertMapState()
    fun assertViewState()
}

/**
 * Hi. If you read this, please refactor all presenters in this way just because it is better and more convenience :)
 */
class GoogleMapPresenterImpl : GoogleMapPresenter {
    private var activity: AppCompatActivity? = null
    private var map: GoogleMap? = null

    override fun bind(activity: AppCompatActivity) {
        this.activity = activity
    }

    override fun setMap(map: GoogleMap) {
        this.map = map
        this.map!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(50.437988, 30.431899), 15f))
    }

    override fun getMap() = map!!

    override fun drawRouteFromMyLocationToPoint(point: LatLng) {
        runAssertSequence()

        val latLngOrigin = LatLng(10.3181466, 123.9029382) // Ayala
        val latLngDestination = LatLng(10.311795, 123.915864) // SM City

        map!!.addMarker(MarkerOptions().position(latLngOrigin).title("Ayala"))
        map!!.addMarker(MarkerOptions().position(latLngDestination).title("SM City"))
        map!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngOrigin, 14.5f))
    }

    override fun drawRouteFromPointToPoint(pointStart: LatLng, pointDestination: LatLng) {
        runAssertSequence()
    }

    override fun drawRouteByLocationPoints(points: List<LatLng>) {
        map!!.addPolyline(
            PolylineOptions().addAll(points).color(ContextCompat.getColor(activity!!, R.color.colorPrimary))
        )
    }

    override fun runAssertSequence() {
        assertViewState()
        assertMapState()
    }

    override fun assertViewState() {
        assertState(activity != null, "View cannot be null")
    }

    override fun assertMapState() {
        assertState(map != null, "Map cannot be null")
    }
}