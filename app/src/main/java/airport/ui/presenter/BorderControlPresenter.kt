package airport.ui.presenter

import airport.ui.view.BorderCtrlView
import airport.ui.view.MainView
import android.app.KeyguardManager
import android.hardware.fingerprint.FingerprintManager
import android.os.CancellationSignal
import java.security.KeyStore
import javax.crypto.Cipher
import javax.crypto.KeyGenerator

interface BorderControlPresenter {

    var fingerprintManager: FingerprintManager
    var keyguardManager: KeyguardManager
    var keyStore: KeyStore
    var keyGenerator: KeyGenerator
    var cipher: Cipher
    var cryptoObject: FingerprintManager.CryptoObject
    var cancellationSignal: CancellationSignal

    val view: BorderCtrlView

    fun bind(view: BorderCtrlView)
    fun getManagers(): Boolean
    fun generateKey()
    fun cipherInit(): Boolean
}