package airport.ui.presenter


import airport.ui.view.PassportControlView

interface PassportControlPresenter {
    fun bind(view: PassportControlView)
}

class PassportControlImpl : PassportControlPresenter {
    private lateinit var view: PassportControlView

    override fun bind(view: PassportControlView) {
        this.view = view
        this.view.setupView()
    }
}