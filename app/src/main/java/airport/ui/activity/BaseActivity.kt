package airport.ui.activity

import android.os.Bundle
import android.support.annotation.LayoutRes

abstract class BaseActivity(@LayoutRes val layout: Int) : PermissionsActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout)
    }
}