package airport.ui.activity.third_level

import airport.airport.R
import airport.ui.activity.BaseActivity
import airport.ui.activity.fourth_level.BaggageDamageActivity
import airport.ui.activity.fourth_level.BaggageLossActivity
import airport.ui.activity.fourth_level.BaggageStorageActivity
import airport.ui.activity.fourth_level.SelfBaggageActivity
import airport.ui.util.*
import airport.ui.view.OtherINformationView
import airport.ui.view.SelfBaggageView
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.res.ResourcesCompat
import android.view.MenuItem
import android.view.View
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_other_information.*

class OtherInformationActivity : BaseActivity(R.layout.activity_other_information), OtherINformationView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupView()
    }

    override fun setupView() {
        setupToolbar()

        cv_self_baggage.setOnClickListener {
            startActivity(Intent(this, SelfBaggageActivity::class.java))
        }
        cv_baggage_damage.setOnClickListener {
            startActivity(Intent(this, BaggageDamageActivity::class.java))
        }
        cv_baggage_lost.setOnClickListener {
            startActivity(Intent(this, BaggageLossActivity::class.java))
        }
        cv_baggage_storage.setOnClickListener {
            startActivity(Intent(this, BaggageStorageActivity::class.java))
        }
    }

    override fun setupToolbar() {
        setSupportActionBar(toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbar.navigationIcon = ResourcesCompat.getDrawable(resources, R.drawable.ic_arrow_back_white_24dp, null)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

}
