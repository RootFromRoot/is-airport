package airport.ui.activity.third_level

import airport.airport.R
import airport.data.util.FingerprintHandler
import airport.ui.activity.BaseActivity
import airport.ui.presenter.BorderControlPresenter
import airport.ui.presenter.implementation.BorderControlImpl
import airport.ui.view.BorderCtrlView
import android.hardware.fingerprint.FingerprintManager
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import org.jetbrains.anko.fingerprintManager

class BorderControlActivity : BaseActivity(R.layout.activity_border_control),
        BorderCtrlView, FingerprintHandler.FingerprintHelperListener {
    override val activity: BorderControlActivity = this

    private val helper = FingerprintHandler(listener = this)
    private val presenter: BorderControlPresenter = BorderControlImpl()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setupToolbar()
        presenter.bind(this)

        if (presenter.getManagers()) {
            presenter.generateKey()
            if (presenter.cipherInit()) {
                presenter.cipher.let {
                    presenter.cryptoObject = FingerprintManager.CryptoObject(it)
                }
                helper.startAuth(fingerprintManager, presenter.cryptoObject)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun authenticationFailed(error: String) {
        Toast.makeText(this, "Authentication Failed", Toast.LENGTH_SHORT).show()
    }

    override fun authenticationSuccess(result: FingerprintManager.AuthenticationResult) {
        Toast.makeText(this, "Authentication Success", Toast.LENGTH_SHORT).show()
    }
}
