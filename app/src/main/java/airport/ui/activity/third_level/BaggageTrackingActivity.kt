package airport.ui.activity.third_level

import airport.airport.R
import airport.ui.activity.BaseActivity
import android.os.Bundle
import android.view.MenuItem

class BaggageTrackingActivity : BaseActivity(R.layout.activity_baggage_tracking) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
