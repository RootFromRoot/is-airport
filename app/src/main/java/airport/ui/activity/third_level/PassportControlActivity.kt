package airport.ui.activity.third_level

// TODO rename package (remove underscore)

import airport.airport.R
import airport.ui.activity.BaseActivity
import airport.ui.activity.fourth_level.RecognizePassportActivity
import airport.ui.presenter.GoogleMapPresenter
import airport.ui.presenter.GoogleMapPresenterImpl
import airport.ui.presenter.PassportControlImpl
import airport.ui.presenter.PassportControlPresenter
import airport.ui.util.KEY_EXTRA_PASSPORT_SCAN_RESULT
import airport.ui.util.KEY_EXTRA_RECOGNIZED_TEXT
import airport.ui.util.REQUEST_CODE_SCAN_PASSPORT
import airport.ui.util.assertState
import airport.ui.view.PassportControlView
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.activity_passport_control.*
import java.util.*

class PassportControlActivity : BaseActivity(R.layout.activity_passport_control), PassportControlView,
    OnMapReadyCallback {
    private var map: GoogleMap? = null

    private val presenterMain: PassportControlPresenter = PassportControlImpl()
    private val presenterMap: GoogleMapPresenter = GoogleMapPresenterImpl()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenterMain.bind(this)
        presenterMap.bind(this)
    }

    override fun setupView() {
        fab_scan_passport.setOnClickListener {
            startActivityForResult(Intent(this, RecognizePassportActivity::class.java), REQUEST_CODE_SCAN_PASSPORT)
        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        assertState(googleMap != null, "Google map is null")

        this.map = googleMap
        presenterMap.setMap(this.map!!)
        presenterMap.drawRouteFromMyLocationToPoint(LatLng(0.0, 0.0))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE_SCAN_PASSPORT && resultCode == Activity.RESULT_OK) {
            processScanResult(data)
        } else {
            processScanError()
        }
    }

    override fun processScanResult(data: Intent?) {
        if (data == null || data.extras == null || TextUtils.isEmpty(data.extras.getString(KEY_EXTRA_RECOGNIZED_TEXT))) {
            processScanError()
        } else {
            processScanSuccess(data.extras.getString(KEY_EXTRA_RECOGNIZED_TEXT))
        }
    }

    override fun processScanSuccess(result: String) {
        cv_bottom_bar.visibility = View.VISIBLE
        Timer().schedule(object : TimerTask() {
            override fun run() {
                Intent().apply {
                    putExtra(KEY_EXTRA_PASSPORT_SCAN_RESULT, result)
                    setResult(Activity.RESULT_OK, this)
                    finish()
                }
            }
        }, 3000)
    }

    override fun processScanError() {
        Toast.makeText(this, "Error. Please, try again", Toast.LENGTH_LONG).show()
    }


}
