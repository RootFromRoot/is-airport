package airport.ui.activity.second_level

import airport.airport.R
import airport.ui.activity.BaseActivity
import airport.ui.activity.third_level.BaggageTrackingActivity
import airport.ui.activity.third_level.BorderControlActivity
import airport.ui.activity.third_level.OtherInformationActivity
import airport.ui.activity.third_level.PassportControlActivity
import airport.ui.util.KEY_EXTRA_PASSPORT_SCAN_RESULT
import airport.ui.util.REQUEST_CODE_PASSPORT_CONTROL
import airport.ui.view.EscortMenuView
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.res.ResourcesCompat
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_escort_menu.*

class EscortMenuActivity : BaseActivity(R.layout.activity_escort_menu), EscortMenuView {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupView()
    }

    override fun setupToolbar() {}

    override fun setupView() {
        cv_passport_ctrl.setOnClickListener {
            startActivityForResult(
                    Intent(this, PassportControlActivity::class.java), REQUEST_CODE_PASSPORT_CONTROL)
        }
        cv_border_ctrl.setOnClickListener { startActivity(Intent(this, BorderControlActivity::class.java)) }
        cv_baggage_navigate.setOnClickListener { startActivity(Intent(this, BaggageTrackingActivity::class.java)) }
        cv_other_info.setOnClickListener { startActivity(Intent(this, OtherInformationActivity::class.java)) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_OK || data == null || data.extras == null) {
            Toast.makeText(this, "Error.Please, try again", Toast.LENGTH_LONG).show()
            return
        }

        when (requestCode) {
            REQUEST_CODE_PASSPORT_CONTROL -> {
                if (!data.extras.containsKey(KEY_EXTRA_PASSPORT_SCAN_RESULT)) {
                    Toast.makeText(this, "Error.Please, try again", Toast.LENGTH_LONG).show()
                    return
                }

                cv_passport_ctrl.setOnClickListener { }
                iv_passport_ctrl_done.visibility = View.VISIBLE
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
