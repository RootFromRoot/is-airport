package airport.ui.activity

import airport.ui.util.PERMISSIONS_MANDATORY
import airport.ui.util.REQUEST_CODE_PERMISSIONS_MANDATORY
import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity

abstract class PermissionsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (!wereMandatoryPermissionsGranted()) {
            requestMandatoryPermissions()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode != REQUEST_CODE_PERMISSIONS_MANDATORY) return

        grantResults.forEach { if (it == PackageManager.PERMISSION_DENIED) requestMandatoryPermissions() }
    }

    private fun wereMandatoryPermissionsGranted(): Boolean {
        for (permission in PERMISSIONS_MANDATORY) {
            if (ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_DENIED) {
                return false
            }
        }

        return true
    }

    private fun requestMandatoryPermissions() {
        requestPermissions(arrayOf(Manifest.permission.CAMERA), REQUEST_CODE_PERMISSIONS_MANDATORY)
    }
}

