package airport.ui.activity.first_level


import airport.airport.R
import airport.data.model.Ticket
import airport.ui.activity.BaseActivity
import airport.ui.presenter.MainPresenter
import airport.ui.presenter.implementation.MainPresenterImpl
import airport.ui.view.MainView
import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

import android.content.Context
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : BaseActivity(R.layout.activity_main), MainView {

    override var activity: MainActivity = this
    private val presenter: MainPresenter = MainPresenterImpl()
    var check: Boolean = false
    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        presenter.bind(this)

        // presenter.fillDB()
        checkIsFirst()
        setupView()
    }

    override fun checkIsFirst() {
        val mPreferences = this.getSharedPreferences("first_time", Context.MODE_PRIVATE)
        val firstTime = mPreferences.getBoolean("firstTime", true)
        if (firstTime) {
            Toast.makeText(this, "First!", Toast.LENGTH_SHORT).show()
            val editor = mPreferences.edit()
            presenter.fillDB()
            editor.putBoolean("firstTime", false)
            editor.commit()
        }
    }

    @SuppressLint("CheckResult")
    override fun setupView() {
        setupBackground()

        lateinit var tickets: List<Ticket>
        lateinit var ticket: Ticket

        presenter.repository.getAll().subscribe { list -> tickets = list }
        btn_register.setOnClickListener {
            val eTTicketNumber: String = et_ticket_number.text.toString()
            Log.i("click", it.toString())
            tickets.forEach { tick ->
                Log.i("ticket", "$tick" + eTTicketNumber)
                if (et_ticket_number.text.isNotEmpty() && eTTicketNumber == tick.ticketNumber) {
                    check = true
                    ticket = tick
                }
            }
            if (check) presenter.startFlightActivity(this, ticket)
            else Toast.makeText(this, "Enter right Ticket Number", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setupBackground() {
        Picasso.get().load(R.drawable.plane_background).into(iv_background)
    }
}

