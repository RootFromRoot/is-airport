package airport.ui.activity.first_level

import airport.airport.R
import airport.data.model.Ticket
import airport.ui.activity.BaseActivity
import airport.ui.activity.second_level.EscortMenuActivity
import airport.ui.presenter.FlightInfoPresenter
import airport.ui.presenter.implementation.FlightInfoImpl
import airport.ui.view.FlightInfoView
import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.res.ResourcesCompat
import kotlinx.android.synthetic.main.activity_flight_info.*

class FlightInfoActivity : BaseActivity(R.layout.activity_flight_info), FlightInfoView {
    override val activity: FlightInfoActivity = this
    override lateinit var ticket: Ticket
    private val presenter: FlightInfoPresenter = FlightInfoImpl()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        presenter.bind(this)
        setupView()
    }

    @SuppressLint("CheckResult")
    override fun setupView() {
        setupToolbar()
        presenter.repository.getById(intent.getIntExtra("id",-1)).subscribe {
            ticket = it
            tv_flight_number.text = it.ticketNumber
            tv_from.text =it.departmentPlace
            tv_departure_time.text = it.departmentTime
            tv_carrier.text = it.carrier
            tv_terminal.text = it.terminal
            tv_gate.text = it.gate
            tv_to.text = it.arrivalPlace
            tv_arrival_time.text = it.arrivalTime
        }
        fab_navigate.setOnClickListener {
            startActivity(Intent(this, EscortMenuActivity::class.java))
        }
    }

    override fun setupToolbar() {
            setSupportActionBar(toolbar)
            setSupportActionBar(toolbar)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowHomeEnabled(true)
            toolbar.navigationIcon = ResourcesCompat.getDrawable(resources, R.drawable.ic_arrow_back_white_24dp,null)

    }
}
