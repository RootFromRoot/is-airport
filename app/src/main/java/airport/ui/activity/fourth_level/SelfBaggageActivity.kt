package airport.ui.activity.fourth_level

import airport.airport.R
import airport.ui.activity.BaseActivity
import airport.ui.presenter.GoogleMapPresenter
import airport.ui.presenter.GoogleMapPresenterImpl
import airport.ui.util.assertState
import airport.ui.view.SelfBaggageView
import android.os.Bundle
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.activity_baggage_damage.*

class SelfBaggageActivity : BaseActivity(R.layout.activity_self_baggage), SelfBaggageView, OnMapReadyCallback {
    private val mapPresenter: GoogleMapPresenter = GoogleMapPresenterImpl()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mapPresenter.bind(this)
        setupView()
    }

    override fun inflateGoogleMap() {
        (supportFragmentManager.findFragmentById(R.id.fragment_map) as SupportMapFragment).getMapAsync(this)
    }

    override fun setupView() {
        inflateGoogleMap()

        iv_back.setOnClickListener {
            finish()
        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        assertState(googleMap != null, "Google map is null")

        mapPresenter.setMap(googleMap!!)
        mapPresenter.drawRouteByLocationPoints(
            arrayListOf(
                LatLng(50.437988, 30.431899),
                LatLng(50.441061, 30.431059),
                LatLng(50.446479, 30.453076),
                LatLng(50.445723, 30.463109),
                LatLng(50.447754, 30.476583),
                LatLng(50.445526, 30.492017),
                LatLng(50.409014, 30.523131),
                LatLng(50.395354, 30.581326),
                LatLng(50.395413, 30.614940),
                LatLng(50.403152, 30.677508),
                LatLng(50.395771, 30.743498),
                LatLng(50.369442, 30.906801),
                LatLng(50.349726, 30.954353),
                LatLng(50.347894, 30.908651),
                LatLng(50.351928, 30.892552)
            )
        )
    }
}

