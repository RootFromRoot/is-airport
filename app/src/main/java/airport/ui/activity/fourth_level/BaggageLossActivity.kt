package airport.ui.activity.fourth_level

import airport.airport.R
import airport.ui.activity.BaseActivity
import airport.ui.view.BaggageLossView
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.res.ResourcesCompat
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_baggage_damage.*

class BaggageLossActivity : BaseActivity(R.layout.activity_baggage_loss), BaggageLossView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupView()
    }

    override fun setupView() {
        iv_back.setOnClickListener {
            finish()
        }
    }

}


