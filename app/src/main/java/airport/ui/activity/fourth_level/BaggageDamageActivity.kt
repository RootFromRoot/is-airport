package airport.ui.activity.fourth_level

import airport.airport.R
import airport.ui.activity.BaseActivity
import airport.ui.util.CustomToolbar
import airport.ui.view.BaggageDamageView
import android.os.Bundle
import android.support.v4.content.res.ResourcesCompat
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_baggage_damage.*

class BaggageDamageActivity : BaseActivity(R.layout.activity_baggage_damage), BaggageDamageView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupView()
    }

    override fun setupView() {
        iv_back.setOnClickListener {
            finish()
        }
    }

}

