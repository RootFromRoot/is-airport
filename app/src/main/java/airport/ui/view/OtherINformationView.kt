package airport.ui.view

interface OtherINformationView {
    fun setupView()
    fun setupToolbar()
}