package airport.ui.view

interface RecognizeView {

    fun setupView()
    fun setupToolbar()

}