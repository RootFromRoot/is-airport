package airport.ui.view

import airport.data.model.Ticket
import airport.ui.activity.first_level.FlightInfoActivity

interface FlightInfoView {
    val activity: FlightInfoActivity
    var ticket:Ticket

    fun setupView()
    fun setupToolbar()
}