package airport.ui.view

interface SelfBaggageView {
    fun setupView()
    fun inflateGoogleMap()
}