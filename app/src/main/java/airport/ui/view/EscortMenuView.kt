package airport.ui.view

interface EscortMenuView {
    fun setupView()
    fun setupToolbar()
}