package airport.ui.view

import airport.ui.activity.first_level.MainActivity

interface MainView {
    val activity: MainActivity

    fun setupView()
    fun checkIsFirst()
}