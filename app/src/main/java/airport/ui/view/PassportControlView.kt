package airport.ui.view

import android.content.Intent

interface PassportControlView {
    fun setupView()

    fun processScanResult(data: Intent?)
    fun processScanSuccess(result: String)
    fun processScanError()
}