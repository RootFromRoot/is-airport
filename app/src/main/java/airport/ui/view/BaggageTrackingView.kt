package airport.ui.view

interface BaggageTrackingView {
    fun setupView()
    fun setupToolbar()
}