package airport.ui.util

import android.Manifest

val PERMISSIONS_MANDATORY = arrayListOf(
        Manifest.permission.CAMERA,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION)