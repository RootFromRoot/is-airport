package airport.ui.util

import airport.airport.R
import android.annotation.SuppressLint
import android.support.annotation.LayoutRes
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar

@SuppressLint("Registered")
class CustomToolbar(@LayoutRes val toolbar: Toolbar) : AppCompatActivity() {
    fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbar.navigationIcon = ResourcesCompat.getDrawable(resources, R.drawable.ic_arrow_back_white_24dp, null)
    }
}