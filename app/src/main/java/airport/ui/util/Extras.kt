package airport.ui.util

const val KEY_EXTRA_RECOGNIZED_TEXT = "recognized_text"
const val KEY_EXTRA_PASSPORT_SCAN_RESULT = "passport_scan_result"