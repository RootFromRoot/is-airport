package airport.ui.util

fun Any.assertState(state: Boolean, message: String) {
    if (!state) throw IllegalStateException(message)
}
